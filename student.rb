require_relative 'person'

class Student < Person
  attr_accessor :classroom # Add attr_accessor for the instance variable `@classroom`

  def initialize(age, classroom, name: 'Unknown', parent_permission: true)
    super(age, name: name, parent_permission: parent_permission)
    @classroom = classroom # Initialize the instance variable `@classroom` with the `classroom` parameter
    classroom.add_student(self) # Add the student to the classroom's `@students` array
  end

  def play_hooky
    "¯\(ツ)/¯"
  end
end