require_relative 'base_decorator'

# Define a new class called CapitalizeDecorator that inherits from the Decorator class.
class CapitalizeDecorator < Decorator
  # Define an initializer that accepts a single argument, which will be assigned to an instance variable called @nameable.
  def initialize(nameable)
    @nameable = nameable
  end

  # Define a method called correct_name that calls the correct_name method of the @nameable object and capitalizes the resulting string.
  def correct_name
    @nameable.correct_name.capitalize
  end
end
