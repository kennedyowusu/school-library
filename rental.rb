class Rental
  attr_accessor :date, :book, :person

  def initialize(date, book, person)
    @date = date
    @book = book
    @person = person

    # Set the rental for the book and person
    book.add_rental(self) # adds the rental instance to the rentals array of the book instance
    person.add_rental(self) # adds the rental instance to the rentals array of the person instance
  end
end


=begin

The Rental class has three instance variables: date, book, and person. In the initialize method, the values of these instance variables are set using the arguments passed to the constructor.

In addition to setting the instance variables, the initialize method also calls two methods to establish the relationship between the Rental, Book, and Person classes:

book.add_rental(self): This adds the current Rental instance to the rentals array of the Book instance, indicating that the book has been rented out.
person.add_rental(self): This adds the current Rental instance to the rentals array of the Person instance, indicating that the person has rented out a book.
This is how the many-to-many relationship is established between the Person and Book classes using the intermediate Rental class.

=end