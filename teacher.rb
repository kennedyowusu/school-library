require_relative 'person' # Imports the `Person` class from the `person.rb` file in the same directory.

class Teacher < Person # Defines a new class `Teacher` that inherits from the `Person` class.
  def initialize(age, specialization, name: 'Unknown', parent_permission: true) # Defines a new constructor for the `Teacher` class that takes `age` and `specialization` as mandatory parameters, and `name` and `parent_permission` as optional parameters with default values.
    super(age, name: name, parent_permission: parent_permission) # Calls the parent class constructor with `age`, `name`, and `parent_permission` as arguments using the `super` keyword.
    @specialization = specialization # Initializes the instance variable `@specialization` with the `specialization` parameter.
  end

  def can_use_services? # Overrides the `can_use_services?` method of the parent class.
    true # Returns `true` unconditionally.
  end
end
