class Person < Nameable
  attr_accessor :name, :age # Creates getter and setter methods for the instance variables `@name` and `@age`.
  attr_reader :id # Creates a getter method for the instance variable `@id`.

  def initialize(age, name: 'Unknown', parent_permission: true) # Constructor method that takes `age` as a mandatory parameter, and `name` and `parent_permission` as optional parameters with default values.
    @id = Random.rand(1..1000) # Initializes the instance variable `@id` with a random number between 1 and 1000.
    @name = name # Initializes the instance variable `@name` with the `name` parameter.
    @age = age # Initializes the instance variable `@age` with the `age` parameter.
    @parent_permission = parent_permission # Initializes the instance variable `@parent_permission` with the `parent_permission` parameter.
  end

  def can_use_services?
    if is_of_age? || @parent_permission # If the person is of age or has parent permission, they can use services, so return true.
      true
    else
      false # Otherwise, they cannot use services, so return false.
    end
  end

  def of_age?
    @age >= 18 # Returns true if the person is 18 years old or older, and false otherwise.
  end
end
