require_relative 'base_decorator'

class TrimmerDecorator < Decorator
 def initialize(nameable)
   @nameable = nameable
 end

 def correct_name
   # @nameable.correct_name.strip
   if @nameable.correct_name.length > 10
     @nameable.correct_name[0..9]
   else
     @nameable.correct_name
    
   end
 end
end