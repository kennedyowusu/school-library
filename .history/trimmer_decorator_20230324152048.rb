require_relative 'base_decorator'

class TrimmerDecorator < Decorator
 def initialize(nameable)
   @nameable = nameable
 end
 