require_relative 'nameable' # Imports the `Person` class from the `person.rb` file in the same directory.

class Decorator < Nameable
  def initialize(nameable)
    @nameable = nameable
  end
  
  def correct_name
    @nameable.correct_name
  end
end