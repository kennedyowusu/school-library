class Nameable
 def correct_name
   NotImplementedError "You must implement the correct_name method"
 end