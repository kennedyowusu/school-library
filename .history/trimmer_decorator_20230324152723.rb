require_relative 'base_decorator'  # Import the base decorator class

class TrimmerDecorator < Decorator  # Define a new decorator class that inherits from the base Decorator class
 def initialize(nameable)  # Constructor method that takes a Nameable object as a parameter and assigns it to an instance variable
   @nameable = nameable
 end

 def correct_name  # Implement the correct_name method that trims the output of @nameable's correct_name method to a maximum of 10 characters
   if @nameable.correct_name.length > 10  # Check if the length of the output of @nameable's correct_name method is greater than 10
     @nameable.correct_name[0..9]  # If it is, return the first 10 characters of the output
   else
     @nameable.correct_name  # Otherwise, return the output as-is
   end
 end
end
