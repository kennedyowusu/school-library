require_relative 'base_decorator'

class Capitalize

 def initialize(nameable)
   @nameable = nameable
 end

 def correct_name
   @nameable.correct_name.capitalize
 end
end