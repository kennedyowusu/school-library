require_relative 'base_decorator'

class TrimmerDecorator < Decorator
 def initialize(nameable)
   @nameable = nameable
 end

 def correct_name
   @nameable.correct_name.strip
 end
end