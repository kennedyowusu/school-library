require_relative 'person' # Imports the `Person` class from the `person.rb` file in the same directory.

class Student < Person # Defines a new class `Student` that inherits from the `Person` class.
  def initialize(age, classroom, name: 'Unknown', parent_permission: true) # Defines a new constructor for the `Student` class that takes `age` and `classroom` as mandatory parameters, and `name` and `parent_permission` as optional parameters with default values.
    super(age, name: name, parent_permission: parent_permission) # Calls the parent class constructor with `age`, `name`, and `parent_permission` as arguments using the `super` keyword.
    @classroom = classroom # Initializes the instance variable `@classroom` with the `classroom` parameter.
  end

  def play_hooky # Defines a new method `play_hooky` that takes no parameters.
    "¯\(ツ)/¯" # Returns the string "¯\(ツ)/¯".
  end
end
glpat-jMhN6HV7MmC6LfdyeiTx