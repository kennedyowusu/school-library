# require the Nameable class, which this class will inherit from
require_relative 'nameable'

# define the Decorator class, which inherits from Nameable
class Decorator < Nameable
  # define the constructor method, which takes a nameable object as a parameter
  def initialize(nameable)
    # assign the nameable object to an instance variable
    @nameable = nameable
  end
  
  # define the correct_name method, which returns the result of calling correct_name on the @nameable object
  def correct_name
    @nameable.correct_name
  end
end
