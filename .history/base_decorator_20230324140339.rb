require_relative 'nameable' # Imports the `Person` class from the `person.rb` file in the same directory.

class Decorator < Nameable

def correct_name
  @name
end
end