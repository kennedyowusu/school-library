require_relative 'base_decorator'

class CapitalizeDecorator < Decorator
 def initialize(nameable)
   @nameable = nameable
 end

 def correct_name
   if test_name(@nameable.correct_name)
     @nameable.correct_name.capitalize
   else
     @nameable.correct_name
   end
 end
end