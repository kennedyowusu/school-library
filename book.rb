class Book
  attr_accessor :title, :author, :rentals

  # Initializes a new instance of Book with a title and an author.
  # Sets up an empty array to hold rentals.
  def initialize(title, author)
    @title = title
    @author = author
    @rentals = []
  end

  # Adds a rental to the rentals array.
  # rental - the rental to add to the array.
  def add_rental(rental)
    @rentals << rental
  end
end


=begin

 The Book class has three instance variables, title, author, and rentals, all of which have getter and setter methods defined by attr_accessor.

The initialize method sets the title and author instance variables to the arguments passed in, and initializes an empty rentals array.

The add_rental method takes a rental object as an argument and adds it to the rentals array.

=end